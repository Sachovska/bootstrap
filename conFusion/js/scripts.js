    $("#carouselButton").click(function(){
            if ($("#carouselButton").children("span").hasClass('fa-pause')) {
                 $("#mycarousel").carousel('pause');
                 $("#carouselButton").children("span").removeClass('fa-pause');
                 $("#carouselButton").children("span").addClass('fa-play');
               }
            else if ($("#carouselButton").children("span").hasClass('fa-play')){
                 $("#mycarousel").carousel('cycle');
                 $("#carouselButton").children("span").removeClass('fa-play');
                 $("#carouselButton").children("span").addClass('fa-pause');                    
               }
          });

        $("#btnLogin").click(function(){
            $("#loginModal").modal("toggle");
        });

        $("#btnReserve").click(function(){
            $("#reserveModal").modal("toggle");
        });

        $("#btnLogClose").click(function(){
            $("#loginModal").modal("hide");
        });

        $("#btnResClose").click(function(){
            $("#reserveModal").modal("hide");
        });

        $("#btnCancle").click(function(){
            $("#reserveModal").modal("hide");
        });

        $("#btnLogCancle").click(function(){
            $("#loginModal").modal("hide");
        });